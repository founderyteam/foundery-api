import gulp from 'gulp'
import gutil from 'gulp-util'
import pump from 'pump'
import mocha from 'gulp-mocha'
import minimist from 'minimist'
import sourcemaps from 'gulp-sourcemaps'
import rename from 'gulp-rename'
import rollup from 'rollup-stream'
import source from 'vinyl-source-stream'
import buffer from 'vinyl-buffer'
import builtins from 'rollup-plugin-node-builtins'
import uglify from 'rollup-plugin-uglify'
import {Server} from 'karma'
import * as path from 'path'
import * as gulpBabelDef from 'gulp-babel'
import babel from 'rollup-plugin-babel'
import gulpCopy from 'gulp-copy'
import babelify from 'babelify'

const gulpBabel = gulpBabelDef.default

const uglifyConfig = {
  output: {
    comments: false,
    beautify: false
  },
  compress: {
    unused: true,
    dead_code: true, // big one--strip code that will never execute
    warnings: false, // good for prod apps so users can't peek behind curtain
    drop_debugger: true,
    conditionals: true,
    evaluate: true,
    sequences: true,
    booleans: true
  }
}

function pumpPromise (pumpList) {
  return new Promise(function (resolve, reject) {
    pump(pumpList, (err) => {
      if (err) { reject(err); return }

      resolve()
    })
  })
}

class Tasks {
  static test () {
    const testFile = minimist(process.argv.slice(2)).on
    if (!testFile) {
      throw new Error('No Test File Specified')
    }

    gutil.log('== Testing ' + testFile + '.test.js Locally ==')

    return new Promise((resolve, reject) => {
      new Server({
        configFile: path.join(__dirname, 'karma.conf.js'),
        singleRun: true
      }, (err, data) => {
        if (err) return reject(err)
        resolve(data)
      }).start()
    })
  }

  static compile () {
    return pumpPromise([
      rollup({
        entry: './src/index.js',
        sourcemap: true,
        external: ['aws-sdk', 'fetch-everywhere'],
        format: 'cjs',
        name: 'Foundery',
        globals: {
          'aws-sdk': 'AWS'
        },
        plugins: [
          builtins(),
          babel({
            exclude: 'node_modules/**',
            presets: [[
              'env',
              {
                'modules': false
              }
            ]],
            plugins: ['external-helpers'],
            // externalHelpers: true,
            babelrc: false
          }),
          uglify(uglifyConfig)
        ]
      }),
      source(('index.js', './src')),
      buffer(),
      sourcemaps.init({loadMaps: true}),
      rename('index.js'),
      sourcemaps.write('.'),
      gulp.dest('build/foundery-api-js')
    ])
  }
}

gulp.task('test', () => {
  return Tasks.test()
})

gulp.task('compile', () => {
  return Tasks.compile()
})
