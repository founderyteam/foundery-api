# Foundery Javascript API

## Installation

Run ```npm i https://alex_brufsky@bitbucket.org/founderyteam/foundery-api-js.git```

## Getting Started

### Importing the API

```
import Foundery from 'foundery-js-api';
const foundery = Foundery(<uri>)
const {User, Idea} = foundery
```

***Make sure to call ```Foundery.getInstance(<instanceid>)``` before anything if the instance is not default***

###Using federated Identity

Call ```user.refreshFederatedIdentity()``` (returns the user object) on an authenticated user
to refresh the credentials.

Once refreshed, you can any aws service that allows federation auth (like s3 uploads, for instance)

The identity is automatically applied when you call ```User.login()```

####Example: Image Upload

This is semi-pseudo code **DO NOT COPY AND PASTE**

```$xslt
User.login("alex@brufsky.org", "token", "linkedin_js")
.then(<parse the response>) 
/* 
response will be in the form:
    {
        result: 'user' || 'newPasswordRequired' || 'mfaRequired',
        info: userObject || {userAttributes, requiredAttributes} || codeDeliveryDetails 
    }
*/
.then((user) => {
    afterLogin()
})


// Call this if more than about 15 minutes after login (or if you are lazy like me, all the time)
function aWhileAfterLogin(user) {
    user.refreshFederatedIdentity().then((user) => {
        performUpload();            
    })
}

function afterLogin() {
    performUpload();
}

function performUpload() {
    AWS.config.credentials.refresh((error) => {
        <handle the error if there is one>

        //do the upload
        this.s3.upload();   

    });
}

```


###Using user.firebaseCredentials()

Returns a promise.resolve(token)

so access with .then((token) => {});

the token is your firebase token.