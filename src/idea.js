/* eslint-disable camelcase */
/* eslint-env node */
/* eslint no-tabs: "off" */
import 'es6-promise/auto'
import 'fetch-everywhere'
import querystring from 'querystring'
import Request from './request'
import Exportable from './exportable'
import { ParseUser, Example, User } from './user'

export function ParseIdea (json) {
  return Idea.parse(json)
}

/** Class representing an Idea. */
export class Idea extends Exportable {
  /**
   * Constructs an idea from a string or number (perferred)
   * @param {string|number} obj - the identifier of the idea
   */
  constructor (obj) {
    const mapping = (ob) => {
      const escapeChars = (query_str) => {
        // console.log(query_str);
        if (query_str) {
          query_str = query_str.replace(/'/g, '’')
        }
        // console.log(query_str);
        return query_str
      }
      return {
        email: ob.thinker ? ob.thinker.email : '',
        id: ob.id,
        whatIf: escapeChars(ob.whatIf),
        fields: ob.fields,
        neededSkills: ob.neededSkills,
        description: escapeChars(ob.description),
        requiredHours: escapeChars(ob.requiredHours),
        boostUntil: ob.boostUntil
      }
    }

    super(mapping)

    // REMIND NOT TO FORGET adding THINKER after making obj
    if (!isNaN(Number(obj))) {
      this.id = Number(obj)
    } else {
      this.whatIf = obj
    }
  }

  /**
   * @deprecated
   */
  static init (uri) {
    Idea.prototype.uri = function () {
      return uri
    }
  }

  /**
   * Creates an idea object from a JS Object
   * @param {object} json the JS Object to parse
   * @return {Idea}
   */
  static parse (json) {
    console.log(json)
    const id = new Idea(json.id)
    id.setProperties(json)

    return id
  }

  /**
   * Creates an example Idea
   * @return {Idea}
   */
  static example () {
    const idea = Idea('What if cats were cunts?')
    idea.thinker = Example()
    idea._fields = ['Agriculture', 'Education']
    idea._neededSkills = ['Marketing', 'Finance']
    idea.description = 'This is a description'
    idea.requiredHours = '12 Hrs / Week'
    idea.id = 0

    return idea
  }

  /**
   * Sets the properties of the current object from a js object
   * @param {object} idea
   */
  setProperties (idea) {
    this.thinker = Object.assign({}, this.thinker || {}, idea.thinker ? User.parse(idea.thinker) : {})
    this.id = idea.id || this.id
    this.whatIf = idea.whatIf || this.whatIf
    this._fields = idea.fields || this._fields || []
    this._neededSkills = idea.neededSkills || this._neededSkills || []
    this.description = idea.description || this.description
    this.requiredHours = idea.requiredHours || this.requiredHours
    this.boostUntil = idea.boostUntil || this.boostUntil
    this.rank = idea.rank || this.rank
  }

  /**
   * Deletes the current idea
   * @return {Promise<object>}
   */
  delete () {
    return new Request(`${this.uri()}/idea/${this.id}`)
      .auth({email: this.thinker.email, token: this.thinker.token})
      .delete({body: this.ex().json().ify()})
  }

  /**
   * Fetches the Idea and assigns it to self
   * @return {Promise<Idea>}
   */
  get () {
    let id = null
    if (this.whatIf) { id = querystring.stringify(this.whatIf) }
    if (this.id) { id = this.id }
    const this2 = this
    return new Request(`${this.uri()}/idea/${id}`)
      .get()
      .then(json => {
        return Object.assign(Idea.parse(json), this2)
      })
  }

  /**
   * Saves the idea
   * @return {Promise<object>}
   */
  save () {
    return new Request(`${this.uri()}/idea/${this.id}`)
      .auth({email: this.thinker.email, token: this.thinker.token})
      .post({body: this.ex().json().ify()})
  }

  create (skills = [], interests = []) {
    const this2 = this
    return new Request(`${this.uri()}/idea/create`)
      .auth({email: this.thinker.email, token: this.thinker.token})
      .post({ body: this.ex().with({ neededSkills: skills, fields: interests }).json().ify() })
      .then(json => {
        console.log(json)
        return Object.assign(Idea.parse(json), this2)
      })
  }

  /**
   * @deprecated
   * @param limit
   * @param start
   */
  matched (limit, start = 0) {
    return new Request(`${this.uri()}/idea/${this.id}/match`)
      .auth({email: this.thinker.email, token: this.thinker.token})
      .post({ body: this.ex().with({limit: limit, start: start}).json().ify() })
      .then(response => response.map(userJson => { return ParseUser(userJson) }))
  }

  /**
   * Searches for ideas based on a query
   * @param {string} q - query
   * @param {number} [limit] - max number of results to return
   * @param {number} [start] - starting position value
   * @return {Promise<Idea[]>}
   */
  search (q, limit = 10, start = 0) {
    return new Request(
      `${this.uri()}/idea/search?q=${
        encodeURIComponent(q)
      }&start=${
        encodeURIComponent(start)
      }&limit=${
        encodeURIComponent(limit)
      }`
    )
      .get()
      .then(response => response.map(ideaJson => { return Idea.parse(ideaJson) }))
  }

  /**
   * Gets the current foundery uri
   * @return {string}
   */
  uri () {
    return ''
  }

  /**
   * fetches the members currently joined to idea
   * @param {number} limit - max results to return
   * @param {number} start - where to start in order
   * @return {Promise<Idea[]>}
   */
  members (limit, start = 0) {
    return new Request(
      `${this.uri()}/idea/${this.id}/users?${
        this.ex().with({limit: limit, start: start}).querystring().ify()
      }`
    )
      .auth({email: this.thinker.email, token: this.thinker.token})
      .get()
      .then(response => response.map(userJson => { return ParseUser(userJson) }))
  }

  /**
   * The fields associated with the object
   * @return {string[]}
   */
  get fields () {
    return this._fields
  }

  /**
   * Associates a list of fields with an object
   * @param {string[]} newValues
   * @return {Promise<object>}
   */
  addFields (newValues) {
    return new Request(`${this.uri()}/idea/${this.id}/interests`)
      .auth({email: this.thinker.email, token: this.thinker.token})
      .post({ body: this.ex().with({interests: newValues}).json().ify() })
      .then(response => {
        this._fields = this._fields || []
        newValues.forEach((val) => {
          const index = this._fields.indexOf(val)
          if (index <= -1) { this._fields.push(val) }
        })

        return response
      })
  }

  /**
   * Dissasociates a list of fields with an object
   * @param {string[]} newValues
   * @return {Promise<object>}
   */
  removeFields (newValues) {
    return new Request(`${this.uri()}/idea/${this.id}/interests`)
      .auth({email: this.thinker.email, token: this.thinker.token})
      .delete({ body: this.ex().with({interests: newValues}).json().ify() })
      .then(response => {
        this._fields = this._fields || []
        newValues.forEach((val) => {
          const index = this._fields.indexOf(val)
          if (index > -1) { this._fields.splice(index) }
        })

        return response
      })
  }

  /**
   * Returns if the idea is currently being promoted
   * @return {boolean}
   */
  get isBoosting () {
    return (this.boostUntil || 0) > Date.now()
  }

  /**
   * Fetches the skills associated with the idea
   * @return {string[]}
   */
  get neededSkills () {
    return this._neededSkills
  }

  /**
   * Associates a skill with the idea
   * @param {string[]} newValues
   * @return {Promise<object>}
   */
  addNeededSkills (newValues) {
    return new Request(`${this.uri()}/idea/${this.id}/skills`)
      .auth({email: this.thinker.email, token: this.thinker.token})
      .post({ body: this.ex().with({skills: newValues}).json().ify() })
      .then(response => {
        this._neededSkills = this._neededSkills || []
        newValues.forEach((val) => {
          const index = this._neededSkills.indexOf(val)
          if (index <= -1) { this._neededSkills.push(val) }
        })

        return response
      })
  }

  /**
   * Disassociates a skill with the idea
   * * @param {string[]} newValues
   * @return {Promise<object>}
   */
  removeNeededSkills (newValues) {
    return new Request(`${this.uri()}/idea/${this.id}/skills`)
      .auth({email: this.thinker.email, token: this.thinker.token})
      .delete({ body: this.ex().with({skills: newValues}).json().ify() })
      .then(response => {
        this._neededSkills = this._neededSkills || []
        newValues.forEach((val) => {
          const index = this._neededSkills.indexOf(val)
          if (index > -1) { this._neededSkills.splice(index) }
        })

        return response
      })
  }

  /**
   * Accept a request to join from user
   * @param {User} user - the user to accept (must have id)
   * @return {Promise<object>}
   */
  acceptRequest (user) {
    return new Request(`${this.uri()}/idea/${this.id}/accept/${user.id}`)
      .auth({email: this.thinker.email, token: this.thinker.token})
      .post({ body: this.ex().json().ify() })
  }

  /**
   * Reject a request to join from user
   * @param {User} user -  the user to reject (must have id)
   * @return {Promise<object>}
   */
  rejectRequest (user) {
    return new Request(`${this.uri()}/idea/${this.id}/reject/${user.id}`)
      .auth({email: this.thinker.email, token: this.thinker.token})
      .post({ body: this.ex().json().ify() })
  }

  /**
   * Removes a user frm the idea
   * @param {User} user - user to remove
   * @return {Promise<object>}
   */
  remove (user) {
    return new Request(`${this.uri()}/idea/${this.id}/remove/${user.id}`)
      .auth({email: this.thinker.email, token: this.thinker.token})
      .post({ body: this.ex().json().ify() })
  }

  /**
   * Fetches the requests on the idea
   * @param {number} [limit] - the max results to return
   * @param {number} [start]  - the starting position
   * @return {Promise<Idea[]>}
   */
  requests (limit = 10, start = 0) {
    return new Request(`${this.uri()}/idea/${this.id}/requests?${
      this.ex().with({limit: limit, start: start}).querystring().ify()
    }`)
      .auth({email: this.thinker.email, token: this.thinker.token})
      .get()
      .then(response => response.map(userJson => { return ParseUser(userJson) }))
  }
}
