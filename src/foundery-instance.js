import { User } from './user'
import { Messages } from './messages'
import { Idea } from './idea'
import Request from './request'

let instance = null

/**
 * FounderyInstance is a singleton representing the current instance
 */
export default class FounderyInstance {
  /**
   * Constructs a new foundery Instance based on a uri.
   * Note: Don't use this constructor to change the uri
   * @param {string} uri - the uri of the api
   * @return {FounderyInstance}
   */
  constructor (uri) {
    if (!instance) {
      this.uri = uri
      this.User = User
      this.Idea = Idea
      this.Messages = Messages

      this.User.prototype.uri = function () {
        return uri
      }

      this.Idea.prototype.uri = function () {
        return uri
      }

      this.Messages.uri = function () {
        return uri
      }
      instance = this
    }

    return instance
  }

  /**
   * Returns the current instance
   * @return {FounderyInstance}
   */
  static shared () {
    return new FounderyInstance(this.uri)
  }

  /**
   * sets the URI of the instance
   * @param {string} uri
   */
  setUri (uri) {
    this.uri = uri

    this.User.prototype.uri = function () {
      return uri
    }

    this.Idea.prototype.uri = function () {
      return uri
    }

    this.Messages.uri = function () {
      return uri
    }
  }

  /**
   * Returns the current cryptographic token describing the instance
   * @return {string}
   */
  getInstanceToken () {
    return this._instanceToken
  }

  /**
   * Returns the instance if there is one
   * @return {Promise<FounderyInstance>}
   */
  getCurrentInstance () {
    return this.getInstance(this._instanceId)
  }

  /**
   * returns the parameters for a CognitoData object
   */
  getPoolData () {
    let first
    if (!this.getInstanceId()) {
      first = this.getDefaultInstance()
    } else {
      first = this.getCurrentInstance()
    }

    return first.then(data => {
      if (!data.cognito) return Promise.reject(new Error('no metadata'))

      return {
        UserPoolId: data.cognito.userPoolId, // Your user pool id here
        ClientId: data.cognito.userPoolClientId // Your client id here
      }
    })
  }

  /**
   * Returns the current instance id
   * @return {string}
   */
  getInstanceId () {
    return this._instanceId
  }

  /**
   * fetches the default instance
   * @param {boolean} [force] -  forces a refresh
   * @return {Promise<object>}
   */
  getDefaultInstance (force = false) {
    return this.getInstance('main', force)
  }

  /**
   * Rerturns the instance metadata specified by instanceName
   * @param {string} instanceName - the name of the instance
   * @param {boolean} [force] -  forces a refresh
   * @return {Promise<object>}
   * {
     toggles: {
       profile: {
         photo: true / false,
         name: true / false,
         headline: true / false
       },
       messages: true / false,
       voting: true / false
     },
     signUpInfo: [
       {
         name: String,
         description: String,
         shortName: String
       }
     ],
     interests: [
     ],
     skills: {
       <category>: [
         "Skill"
       ]
     }
   }
   */
  getInstance (instanceName, force = false) {
    if (this._instanceMetaData && force === false) {
      console.log(this._instanceMetaData)
      return Promise.resolve(this._instanceMetaData)
    }

    return new Request(`${this.uri}/info/${instanceName}`, 'instance_details')
      .get()
      .then(json => {
        this._instanceToken = json.instanceToken
        this._instanceId = json.instanceId

        delete json.instanceToken
        delete json.instanceId
        this._instanceMetaData = json

        return this._instanceMetaData
      })
  }
}
