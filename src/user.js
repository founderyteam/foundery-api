/* eslint-disable camelcase */
/* eslint-env node */
/* eslint no-unused-vars: "warn" */
/* eslint no-tabs: "off" */
/* eslint no-undef: "warn" */
import 'es6-promise/auto'
import 'fetch-everywhere'
import 'babel-polyfill'
import Request from './request'
import Exportable from './exportable'
// import * as AWS from 'aws-sdk'
import AWS from 'aws-sdk/browser'
import {Config, CognitoIdentityCredentials} from 'aws-sdk'
import S3 from 'aws-sdk/clients/s3'
import {CognitoUserPool, CognitoUser, AuthenticationDetails} from 'amazon-cognito-identity-js'
import {ParseIdea} from './idea'
import {generate as ShortIdGenerate} from 'shortid'
import FounderyInstance from './foundery-instance'

export function Example () {
  return User.example()
}

export function ParseUser (json) {
  return User.parse(json)
}

function mapIdeaFromResponse (resp) {
  return resp.map(function (idea) {
    return ParseIdea(idea)
  })
}

/** Class representing an Idea. */
export class User extends Exportable {
  /**
   * User Constructor
   * @param {string|number} obj - the id or email of the user
   */
  constructor (obj) {
    const mapping = ob => {
      const escapeChars = (query_str) => {
        // console.log(query_str);
        if (query_str) {
          query_str = query_str.replace(/'/g, '’')
        }
        // console.log(query_str);
        return query_str
      }

      return {
        email: ob.email,
        id: ob.id,
        name: escapeChars(ob.name),
        interests: ob.interests,
        skills: ob.skills,
        bio: escapeChars(ob.bio),
        profile_pic: ob.profile_pic,
        url: escapeChars(ob.url),
        headline: escapeChars(ob.headline),
        groups: ob.groups,
        token: ob.token || '',
        funds: ob.funds,
        viewedExplainer: ob.viewedExplainer,
        utcOffset: ob.utcOffset,
        social: ob.social,
        settings: ob.settings,
        metadata: ob.metadata
      }
    }
    super(mapping)

    if (!isNaN(Number(obj))) {
      this.id = Number(obj)
    } else {
      this.email = obj
    }
  }

  /**
   * Sets the uri on the user static object
   * @param {string} uri - the uri
   */
  static init (uri) {
    User.prototype.uri = function () {
      return uri
    }
  }

  /**
   * Returns an example user object
   * @returns {User}
   */
  static example () {
    const user = new User('alex@brufsky.org')
    user.name = 'Alex Brufsky'
    user._interests = ['Agriculture', 'Education']
    user._skills = ['Marketing', 'Finance']
    user.bio = 'This is my bio'
    user.profile_pic = 'http://lorempixel.com/200/200/'
    user.url = 'http://linkedin.com'
    user.headline = 'I\'m a Professional Idiot'
    user.id = 0
    user.groups = []

    return user
  }

  /**
   * Maps a js object into a user object
   * @param {object} json - the json corresponding to the user properties
   * @returns {User}
   */
  static parse (json) {
    // console.log(json)
    const us = new User(json.id)
    us.setProperties(json)
    return us
  }

  /**
   * Sets properties on parameter obj to the current user
   * @param {object} obj - parameters to set
   */
  setProperties (obj) {
    this.email = obj.email || this.email
    this.id = obj.id || this.id
    this.name = obj.name || this.name
    this._interests = obj.interests || this._interests || []
    this._skills = obj.skills || this._skills || []
    this.bio = obj.bio || this.bio
    this.profile_pic = obj.profile_pic || this.profile_pic
    this.url = obj.url || this.url
    this.headline = obj.headline || this.headline
    this.groups = obj.groups || this.groups || []
    this.funds = obj.funds || this.funds || 0
    this.viewedExplainer = obj.viewedExplainer || this.viewedExplainer || false
    this.rank = obj.rank || this.rank || 0
    this.utcOffset = obj.utcOffset || this.utcOffset || '-0:00'

    obj.settings = obj.settings || {}
    obj.social = obj.social || {}
    obj.metadata = obj.metadata || {}

    this.settings = this.settings || {}
    this.social = this.social || {}
    this.metadata = this.metadata || {}

    this.social = {
      facebook: obj.social.facebook || this.social.facebook || '',
      instagram: obj.social.instagram || this.social.instagram || '',
      linkedin: obj.social.linkedin || this.social.linkedin || '',
      twitter: obj.social.twitter || this.social.twitter || '',
      url: obj.social.url || this.social.url || ''
    }

    this.settings = {
      dailySummaryEmailNotification: obj.settings.dailySummaryEmailNotification || this.settings.dailySummaryEmailNotification || false,
      founderyUpdatesEmailNotification: obj.settings.founderyUpdatesEmailNotification || this.settings.founderyUpdatesEmailNotification || false,
      joinRequestEmailNotification: obj.settings.joinRequestEmailNotification || this.settings.joinRequestEmailNotification || false,
      newChatEmailNotification: obj.settings.newChatEmailNotification || this.settings.newChatEmailNotification || false,
      newMessageEmailNotification: obj.settings.newMessageEmailNotification || this.settings.newMessageEmailNotification || false
    }

    this.metadata = obj.metadata || this.metadata
  }

  /**
   * Deletes the current user object from the database, not from cognito. Unsafe
   */
  delete () {
    return new Request(`${this.uri()}/user/${this.id}`)
      .auth({email: this.email, token: this.token})
      .delete({body: this.ex().json().ify()})
  }

  /**
   * Gets the firebase credentials for a user
   * @returns {Promise<any>}
   */
  firebaseCredentials () {
    if (!this.email) {
      return Promise.reject(new Error('No Email on User'))
    }

    return new Request(`${this.uri()}/auth/google?email=${this.email}`)
      .auth({email: this.email, token: this.token})
      .post({
        body: JSON.stringify({
          token: this.token,
          email: this.email
        })
      }).then(resp => {
        return resp.token || resp
      })
  }
  // firebaseCredentialsOld () {
  //   return fetch(`https://jtwpr5ot91.execute-api.us-east-1.amazonaws.com/dev/google?email=${this.email}`,
  //     {
  //       method: 'GET',
  //       headers: {
  //         'x-auth-email': this.email,
  //         'x-is-user-pool': false,
  //         'x-web-token': this.token,
  //         'Content-Type': 'application/json'
  //       }
  //     }).then(resp => resp.json())
  //     .then((resp) => {
  //       if (!resp) {
  //         return Promise.reject(new Error('Empty Response'))
  //       } else {
  //         const body = JSON.parse(resp)
  //         if (!body.success) {
  //           return Promise.reject(new Error('Unsuccessful'))
  //         } else {
  //           return body
  //         }
  //       }
  //     }).then((response) => {
  //       //   console.log(response)
  //       if (response && response.payload && response.payload.token) {
  //         return response.payload.token
  //       } else {
  //         return Promise.reject(new Error('No Token'))
  //       }
  //     })
  // }

  /**
   * Downloads and sets the user properties of the current user object
   * @returns (Promise<User>)
   */
  get () {
    const this2 = this
    return new Request(`${this.uri()}/user/${this.email || this.id}`)
      .get()
      .then(json => {
        return Object.assign(User.parse(json), this2)
      })
  }

  /**
   * computes the number of chats from the groups object
   * @returns (Number)
   */
  get numChats () {
    return this.groups ? this.groups.length || 0 : 0
  }

  /**
   * Makes cognito User
   * @returns {Promise<object>} CognitoUser
   */
  async makeCognitoUser () {
    const poolData = await FounderyInstance.shared().getPoolData()
    const userPool = new CognitoUserPool(poolData)
    const userData = {
      Username: this.email,
      Pool: userPool
    }

    return new CognitoUser(userData)
  }

  /**
   * refreshes the cognito identity
   * @returns (Promise<User>)
   */
  refreshFederatedIdentity () {
    return new Request(`${new User().uri()}/auth/identity`)
      .auth({email: this.email, token: this.token})
      .post({
        body: JSON.stringify({
          email: this.email,
          auth_token: this.token
        })
      })
      .then((response) => {
        this.iam_id = response.iam_id
        this.iam_token = response.iam_token
        // apply token to AWS global
        AWS.config = new Config({
          region: 'us-east-1',
          credentials: new CognitoIdentityCredentials({
            IdentityId: response.iam_id,
            IdentityPoolId: response.iam_id,
            Logins: {
              'cognito-identity.amazonaws.com': response.iam_token
            }
          }, {
            region: 'us-east-1'
          })
        })

        AWS.config.credentials.expired = true

        return Promise.resolve(this)
      })
  }

  /** uploads a file to s3
   * @param {object} options
   * @param {File} options.file [file] the form data file to upload
   * @param {string} options.bucket - the bucket to upload to
   * @param {boolean} options.make_public - determines if object is web accessible
   * @returns {{key: string, bucket: string}}
   */
  upload (options) {
    let {file, bucket, make_public, cache_control, file_type} = options

    make_public = make_public || false
    cache_control = cache_control || 'max-age=120, public'
    file_type = file_type || file.type || 'application/octet-stream'

    return this.refreshFederatedIdentity()
      .then(() => {
        if (!AWS.config || !file || !bucket) {
          return Promise.reject(new Error('No creds, file, or Bucket'))
        }

        const s3 = new S3({credentials: AWS.config.credentials, bucket: bucket})
        const fileName = ShortIdGenerate() + ShortIdGenerate() + '.' + file.name.split('.').reverse()[0]
        console.log(fileName)
        return s3.upload({
          Key: fileName,
          Bucket: bucket,
          Body: file,
          GrantFullControl: 'uri=http://acs.amazonaws.com/groups/global/AllUsers',
          GrantRead: 'uri=http://acs.amazonaws.com/groups/global/AuthenticatedUsers',
          ContentType: file_type,
          CacheControl: cache_control,
          Metadata: {
            'cache-control': cache_control,
            'content-type': file_type
          },
          Tagging: this.email ? `email=${this.email}&id=${this.id}` : `id=${this.id}`
        }).promise()
          .then(data => {
            return {key: fileName, bucket: bucket, url: data.Location}
          })
          .catch(e => {
            console.error(e)
            return Promise.reject(e)
          })
      })
  }

  /**
   * Saves the current user object
   * @returns (Promise<any>)
   */
  save () {
    return new Request(`${this.uri()}/user/${this.id}`)
      .auth({email: this.email, token: this.token})
      .post({body: this.ex().json().ify()})
  }

  /**
   * Creates a new user
   * @returns (Promise<any>)
   */
  create () {
    const this2 = this
    return new Request(`${this.uri()}/user/create`)
      .auth({email: this.email, token: this.token})
      .post({body: this.ex().json().ify()})
      .then(json => {
        return Object.assign(User.parse(json), this2)
      })
  }

  /**
   * Returns the ideas that match with the current user object
   * @param {number} limit - the number of results to return
   * @param {number} start  - the starting index of the search
   * @returns (Promise<Array>) Array of ideas
   */
  matched (limit, start = 0) {
    return new Request(`${this.uri()}/user/${this.id}/match`)
      .auth({email: this.email, token: this.token})
      .post({
        body: this.ex().with({limit: limit, start: start}).json().ify()
      })
      .then(mapIdeaFromResponse)
  }

  uri () {
    return ''
  }

  /**
   * Returns the projects on or joined to the current user
   * @param {number} limit - the number of results to return
   * @param {number} start  - the starting index of the search
   * @returns (Promise<Array>) Array of ideas
   */
  projects (limit, start = 0) {
    return new Request(
      `${this.uri()}/user/${this.id}/projects?${
        this.ex().with({limit: limit, start: start}).querystring().ify()
      }`
    )
      .auth({email: this.email, token: this.token})
      .get()
      .then(mapIdeaFromResponse)
  }

  get interests () {
    return this._interests
  }

  /**
   * Adds the newValues to the current user's interests
   * @param {string[]} newValues - new interests to add
   * @returns (Promise<any>)
   */
  addInterests (newValues) {
    return new Request(`${this.uri()}/user/${this.id}/interests`)
      .auth({email: this.email, token: this.token})
      .post({body: JSON.stringify({interests: newValues, email: this.email})})
      .then(response => {
        this._interests = this._interests || []
        newValues.forEach((val) => {
          const index = this._interests.indexOf(val)
          if (index <= -1) {
            this._interests.push(val)
          }
        })
        return response
      })
  }

  /**
   * removes the newValues from the current user's interests
   * @param {string[]} newValues - new interests to remove
   * @returns (Promise<any>)
   */
  removeInterests (newValues) {
    return new Request(`${this.uri()}/user/${this.id}/interests`)
      .auth({token: this.token, email: this.email})
      .delete({body: JSON.stringify({interests: newValues, email: this.email})})
      .then(response => {
        newValues.forEach((val) => {
          this._interests = this._interests || []

          const index = this._interests.indexOf(val)
          if (index > -1) {
            this._interests.splice(index)
          }
        })

        return response
      })
  }

  get skills () {
    return this._skills
  }

  /**
   * Adds the newValues to the current user's skills
   * @param {string[]} newValues - new skills to add
   * @returns (Promise<any>)
   */
  addSkills (newValues) {
    return new Request(`${this.uri()}/user/${this.id}/skills`)
      .auth({email: this.email, token: this.token})
      .post({body: JSON.stringify({skills: newValues, email: this.email})})
      .then(response => {
        this._skills = this._skills || []
        newValues.forEach((val) => {
          const index = this._skills.indexOf(val)
          if (index <= -1) {
            this._skills.push(val)
          }
        })
        return response
      })
  }

  /**
   * removes the newValues from the current user's skills
   * @param {string[]} newValues - new skills to remove
   * @returns (Promise<any>)
   */
  removeSkills (newValues) {
    return new Request(`${this.uri()}/user/${this.id}/skills`)
      .auth({email: this.email, token: this.token})
      .delete({body: JSON.stringify({skills: newValues, email: this.email})})
      .then(response => {
        this._skills = this._skills || []
        newValues.forEach((val) => {
          const index = this._skills.indexOf(val)
          if (index > -1) {
            this._skills.splice(index)
          }
        })
        return response
      })
  }

  /**
   * Requests to join an idea
   * @param {number} idea - the id of the idea to join
   */
  requestToJoin (idea) {
    return new Request(`${this.uri()}/user/${this.id}/ask/${idea.id}`)
      .auth({email: this.email, token: this.token})
      .post({body: this.ex().json().ify()})
  }

  acceptInvite (idea) {
    return new Request(`${this.uri()}/user/${this.id}/accept/${idea.id}`)
      .auth({email: this.email, token: this.token})
      .post({body: this.ex().json().ify()})
  }

  requests (limit, start = 0) {
    return new Request(`${this.uri()}/user/${this.id}/requests?${
      this.ex().with({limit: limit, start: start}).querystring().ify()
    }`)
      .auth({email: this.email, token: this.token})
      .get()
      .then(mapIdeaFromResponse)
  }

  /**
   * Searches for ideas based on a query
   * @param {string} q - the query
   * @param {number} limit - the number of results to return
   * @param {number} start  - the offset of the result index
   */
  search (q, limit, start = 0) {
    return new Request(
      `${new User().uri()}/idea/search?q=${encodeURIComponent(q)}&start=${encodeURIComponent(start)}&limit=${encodeURIComponent(limit)}`
    )
      .get()
      .then(mapIdeaFromResponse)
  }

  // Votes

  /**
   * Votes on an Idea
   * @param {Idea} idea - the idea to vote on
   * @param {number} [strength] - value from 1 to 4
   * @return {Promise<object>} - returns a Vote object with an id and strength
   */
  vote (idea, strength = 1) {
    if (!idea || !idea.id) {
      return Promise.reject(new Error('No Idea Id'))
    }

    return new Request(`${new User().uri()}/user/${this.id || this.email}/vote/${idea.id}`)
      .auth({email: this.email, token: this.token})
      .post({body: this.ex().with({strength: strength}).json().ify()})
  }

  /**
   * Gets the ideas that the user voted for along with strength of vote
   * @param {number} [start]  -  starting position
   * @param {number} [limit] -  max number of results
   */
  votes (start = 0, limit = -1) {
    return new Request(`${new User().uri()}/user/${this.id || this.email}/vote?start=${start}&limit=${limit}`)
      .get()
      .then(
        votes => votes.map(
          (vote) => ({
            strength: vote.strength,
            idea: ParseIdea(vote.idea)
          })
        )
      )
  }

  // Funds

  /**
   * Purchases Currency
   * @param {number} amount - the amount of currency to purchase
   * @param {string} stripeToken - the token returned from stripe
   * @returns {Promise<object>}
   * @example
   * user.buyCurrency(1, 'not a real token')
   * .then((emptySuccess) => {
   *     console.log(emptySuccess)
   *     return true
   *   }).catch((e) => {
   *    console.error(e)
   *     return e
   *   })
   */
  buyCurrency (amount, stripeToken) {
    return new Request(`${new User().uri()}/user/${this.id || this.email}/currency/buy`)
      .auth({email: this.email, token: this.token})
      .post({body: this.ex().with({stripeToken, amount}).json().ify()})
  }

  /**
   * boosts an idea drawing funds from the current user
   * @param {number} idea - the id of the idea to boost
   * @param {number} amount - amount of funds to use
   * @return {Promise<object>}
   * @example
   * user.boost(idea, 1)
   *  .then((emptySuccess) => {
   *    console.log(emptySuccess)
   *     return true
   *  })
   *  .catch((e) => {
   *     console.error(e)
   *     return e
   *  })
   */
  boost (idea, amount) {
    return new Request(`${new User().uri()}/user/${this.id || this.email}/currency/use`)
      .auth({email: this.email, token: this.token})
      .post({
        body: this.ex().with({
          ideaId: idea.id,
          amount: amount,
          action: 'boost'
        }).json().ify()
      })
  }

  // User.login(email, password).then((user) => { return user.get(); }).then((user) => {

  // })
  // MARK: - LOGIN STUFF

  /**
   * Logs a user in with a user and password or a user and token
   * @param {string} email - the email of the user
   * @param {string} key - either the password or the login_token
   * @param {string} provider - either user_pool for cognito or linkedin_js for linkedin
   * @returns (Promise<LoginResponse>)
   */
  static async login (email, key, provider) {
    const fetchFounderyCreds = (passed) => {
      const {options, shouldContinue} = passed
      if (!shouldContinue) {
        return Promise.resolve(options)
      }

      const {email, auth_token, provider} = options
      // console.log(options);
      return new Request(`${new User().uri()}/auth/login`)
        .post({
          body: JSON.stringify({
            email: email,
            auth_token: auth_token,
            login_provider: provider
          }),
          headers: {'Content-Type': 'application/json'}
        })
        .then((response) => {
          const us = new User(email)
          us.token = response.token
          us.provider = provider
          us.iam_id = response.iam_id
          us.iam_token = response.iam_token

          // apply token to AWS global
          // this.config.re = 'us-east-1'
          AWS.config = new Config({
            region: 'us-east-1',
            credentials: new CognitoIdentityCredentials({
              // IdentityPoolId: 'us-east-1:4146893d-9250-4c16-9f48-23336c4ccc96', // your identity pool id here,
              IdentityId: response.iam_id,
              Logins: {
                'cognito-identity.amazonaws.com': response.iam_token
              }
            }, {region: 'us-east-1'})
          })

          return Promise.resolve({result: 'user', info: us})
        })
    }

    if (provider === 'user_pool') {
      const poolData = await FounderyInstance.shared().getPoolData()
      const cognitoData = await FounderyInstance.shared().getCurrentInstance()
      if (!cognitoData || !cognitoData.cognito || !cognitoData.cognito.identityPoolId) return Promise.reject(new Error('No Cognito Data'))
      const identityPoolId = cognitoData.cognito.identityPoolId

      const userPool = new CognitoUserPool(poolData)

      const authenticationData = {
        Username: email,
        Password: key
      }

      const authenticationDetails = new AuthenticationDetails(authenticationData)

      const userData = {
        Username: email,
        Pool: userPool
      }

      const cognitoUser = new CognitoUser(userData)
      return new Promise((resolve, reject) => {
        cognitoUser.authenticateUser(authenticationDetails, {
          onSuccess: function (result) {
            // console.log('access token + ' + result.getAccessToken().getJwtToken());

            const key = 'cognito-idp.us-east-1.amazonaws.com/' + poolData.UserPoolId
            const Logins = {}
            Logins[key] = result.getIdToken().getJwtToken()
            // POTENTIAL: Region needs to be set if not already set previously elsewhere.
            AWS.config = new Config({
              region: 'us-east-1',
              credentials: new CognitoIdentityCredentials({
                IdentityPoolId: identityPoolId, // your identity pool id here
                Logins: Logins
              }, {
                region: 'us-east-1'
              })
            })

            // console.log(result)
            resolve({options: {email, auth_token: result.getIdToken().getJwtToken(), provider}, shouldContinue: true})

            // Instantiate aws sdk service objects now that the credentials have been updated.
            // example: var s3 = new AWS.S3();
          },

          onFailure: function (err) {
            reject(err)
          },
          mfaRequired: function (codeDeliveryDetails) {
            resolve({options: {result: 'mfaRequired', info: codeDeliveryDetails}, shouldContinue: false})
          },
          newPasswordRequired: function (userAttributes, requiredAttributes) {
            resolve({options: {result: 'newPasswordRequired', info: {userAttributes, requiredAttributes}}, shouldContinue: false})
          }
        })
      }).then(fetchFounderyCreds)
    } else {
      return fetchFounderyCreds({options: {email, auth_token: key, provider}, shouldContinue: true})
    }
  }

  // static loginOld (email, password) {
  //   return new Promise((resolve, reject) => {
  //     // yay I can depricate this
  //     const userPool = new CognitoUserPool(poolData)
  //
  //     const authenticationData = {
  //       Username: email,
  //       Password: password
  //     }
  //
  //     const authenticationDetails = new AuthenticationDetails(authenticationData)
  //
  //     const userData = {
  //       Username: email,
  //       Pool: userPool
  //     }
  //
  //     var cognitoUser = new CognitoUser(userData)
  //     cognitoUser.authenticateUser(authenticationDetails, {
  //       onSuccess: function (result) {
  //         // console.log('access token + ' + result.getAccessToken().getJwtToken());
  //
  //         // POTENTIAL: Region needs to be set if not already set previously elsewhere.
  //         Config.region = 'us-east-1'
  //
  //         Config.credentials = new CognitoIdentityCredentials({
  //           IdentityPoolId: 'us-east-1:4146893d-9250-4c16-9f48-23336c4ccc96', // your identity pool id here
  //           Logins: {
  //             'cognito-idp.us-east-1.amazonaws.com/us-east-1_XnqNMCHyg': result.getIdToken().getJwtToken()
  //           }
  //         })
  //
  //         const us = new User(email)
  //         us.cognitoUser = cognitoUser
  //         us.token = result.getIdToken().getJwtToken()
  //         resolve(us)
  //
  //         // Instantiate aws sdk service objects now that the credentials have been updated.
  //         // example: var s3 = new AWS.S3();
  //       },
  //
  //       onFailure: function (err) {
  //         reject(err)
  //       }
  //
  //     })
  //   })
  // }

  // just sends code. use cognito user to confirm change
  static async forgotPassword (email) {
    const poolData = await FounderyInstance.shared().getPoolData()
    const userPool = new CognitoUserPool(poolData)
    const userData = {
      Username: email,
      Pool: userPool
    }

    const cognitoUser = new CognitoUser(userData)

    return new Promise((resolve, reject) => {
      cognitoUser.forgotPassword({
        onSuccess: function () {
          resolve()
        },
        onFailure: function (err) {
          reject(err)
        }
      })
    })
  }

  static async register (email, password) {
    const poolData = await FounderyInstance.shared().getPoolData()
    const userPool = new CognitoUserPool(poolData)

    return new Promise((resolve, reject) => {
      userPool.signUp(email, password, [], null, function (err, result) {
        if (err) {
          reject(err)
          return
        }

        const cognitoUser = result.user
        // console.log('user name is ' + cognitoUser.getUsername());

        resolve(cognitoUser)
      })
    })
  }

  static resendConfirmation (cognitoUser) {
    return new Promise((resolve, reject) => {
      cognitoUser.resendConfirmationCode(function (err, result) {
        if (err) {
          reject(err)
          return
        }
        // console.log('call result: ' + result);
        resolve()
      })
    })
  }

  /**
   * Changes the password of an existing user
   * @param oldPassword
   * @param newPassword
   * @return {Promise<any>}
   */
  async changePassword (oldPassword, newPassword) {
    const authenticationData = {
      Username: this.email,
      Password: oldPassword
    }

    const authenticationDetails = new AuthenticationDetails(authenticationData)
    const cognitoUser = await this.makeCognitoUser()
    return new Promise((resolve, reject) => {
      cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: function (result) {
          // console.log(result)
          resolve(cognitoUser)
        },

        onFailure: function (err) {
          reject(err)
        },
        mfaRequired: function (codeDeliveryDetails) {
          reject(new Error('User hasn\'t confirmed email'))
        },
        newPasswordRequired: function (userAttributes, requiredAttributes) {
          reject(new Error('User hasn\'t set a new password through confirm forgot password'))
        }
      })
    }).then(u => {
      return new Promise((resolve, reject) => {
        u.changePassword(oldPassword, newPassword, function (err, result) {
          if (err) {
            reject(err)
            return
          }

          resolve(result)
        })
      })
    })
  }

  /**
   * Use this if you get a newPassword Required error on login
   * @param {string} code - verification code
   * @param {string} newPassword - the new password
   */
  async confirmNewPassword (code, newPassword) {
    const cognitoUser = await this.makeCognitoUser()
    return new Promise((resolve, reject) => {
      cognitoUser.confirmPassword(code, newPassword, {
        onSuccess: function () {
          resolve()
        },
        onFailure: function (err) {
          reject(err)
        }
      })
    })
  }

  // User.confirm(email, code).then((user) => user.get()).then((user) => {

  // })
  static async confirm (email, code) {
    const poolData = await FounderyInstance.shared().getPoolData()

    const userPool = new CognitoUserPool(poolData)
    const userData = {
      Username: email,
      Pool: userPool
    }

    const cognitoUser = new CognitoUser(userData)

    return new Promise((resolve, reject) => {
      cognitoUser.confirmRegistration(code, true, function (err, result) {
        if (err) {
          reject(err)
          return
        }

        resolve()
      })
    })
  }
}

/**
 * @typedef {object} LoginResponse
 * @property {string} result 'user' || 'newPasswordRequired' || 'mfaRequired',
 * @property {user|{userAttributes:object, requiredAttributes:string[]}|object} info user object or recovery info
 */
