/* eslint-disable camelcase */
/* eslint-env node */
/* eslint no-unused-vars: "warn" */
/* eslint no-tabs: "off" */
import 'es6-promise/auto'
import 'fetch-everywhere'
import {device} from 'aws-iot-device-sdk'
import Request from './request'

/**
 * Provides the messaging component to foundery
 */
export class Messages {
  /**
   * constructs an iOT client
   * @param options
   * @param {string} options.region - the aws region
   * @param {User} options.user - the user associated with the client
   * @param {string[]} options.channels
   * @param {function} options.onConnect - callback on successful connect
   * @param {function(json: object)} options.onMessage - callback on message received
   * @param {function} options.onClose - callback on close of client
   * @return {Promise<Messages>}
   * @example
   * 	Messages.client({
   *      user: aUser,
   *      region: 'us-east-1',
   *      channels: ['test_channel'], // optional
   *      onMessage: (messageJson) => { // optional, but will print to console if not set
   *        console.log(messageJson) // see reference for the messageJson structure
   *      }
   *    }).then((client) => { client.connect(); }) // required. Connects client to service. Save the client to a variable if you want to be able to end connection
   *    .catch((e) => {
   *      console.error(e)
   *    })
   */
  static client (options) {
    if (!options.region || !options.user) {
      return Promise.reject(new Error('No Region or No User'))
    }

    const client = new Messages()
    client.region = options.region
    client.user = options.user
    client.connected = false
    client.topics = options.channels || []
    client.onConnect = options.onConnect || (() => {
      client.connected = true
      client.topics.forEach((topic) => {
        client.client.subscribe(topic)
        console.log('Connected to ' + topic)
      })
    })
    client.onMessage = options.onMessage || ((json) => {
      console.log(
        'Message: ' +
        json.message +
        ' recieved on channel: ' +
        (json.channel || json.group_name)
      )
    })
    client.onClose = options.onClose || (() => {
      client.connected = false
      console.log('Connection Failed')
    })

    return new Request(`${Messages.uri()}/chat/auth`)
      .auth({email: options.user.email, token: options.user.token})
      .post({
        body: JSON.stringify({
          email: options.user.email
        })
      })
      .then(function (endpointData) {
        client.iotEndpoint = endpointData.iotEndpoint
        client.accessKey = endpointData.accessKey
        client.secretKey = endpointData.secretKey
        client.sessionToken = endpointData.sessionToken

        return Promise.resolve(client)
      })
  }

  /**
   * connects client instance to the server
   */
  connect () {
    // connect
    // console.log(this)
    const client = device({
      region: this.region,
      protocol: 'wss',
      accessKeyId: this.accessKey,
      secretKey: this.secretKey,
      sessionToken: this.sessionToken,
      port: 443,
      host: this.iotEndpoint
    })

    console.log(client)
    client.on('connect', this.onConnect)

    const messageRecvFunc = this.onMessage
    client.on('message', (channel, message) => {
      let jsonMessage
      try {
        jsonMessage = JSON.parse(message.toString())
      } catch (e) {
        console.error(e, message.toString(), channel)
      }

      messageRecvFunc(jsonMessage || {success: false, message: "couldn't read message", channel: channel})
    })

    client.on('close', this.onClose)

    this.client = client
  }

  /**
   * Connects the client to a channel (group)
   * @param {string} channel - the channel to connect to
   * @example
   * // (Assuming a local variable client)
   * client.connectToChannel('test_channel2')
   */
  connectToChannel (channel) {
    this.client.subscribe(channel)
    console.log('Connected to ' + channel)
  }

  /**
   * Disconnects the client to a channel (group)
   * @param {string} channel - the channel to disconnect from
   * @example
   * // (Assuming a local variable client)
   * client.disconnectFromChannel('test_channel2')
   */
  disconnectFromChannel (channel) {
    this.client.unsubscribe(channel)
    console.log('Disconnected from ' + channel)
  }

  /**
   * disconnects the client from all channels and the server
   * @example
   * client.disconnect()
   */
  disconnect () {
    this.client.end()
    console.log('Disconnected from socket')
  }

  /**
   * Returns the messages on a channel
   * @param options
   * @params {string} options.channel - the name of the channel
   * @params {User} options.user - the user associated with the channel
   * @params {number} options.limit - the max number of results to return (optional)
   * @params {number} options.before - recieve messages before this unix timestamp (optional)
   * @params {number} options.since - recieve messages since this unix timestamp
   * @return {Promise<Message>}
   * @example
   * Messages.forChannel({user: aUser, channel: 'channel'})
   *  .then((messages) => {
   *    console.log(messages)
   *
   *  }).catch((e) => {
   *    console.error(e)
   *  })
   */
  static forChannel (options) {
    if (!options || !options.channel || !options.user) {
      return Promise.reject(new Error('No Channel, User, or Options'))
    }

    const channel = options.channel
    const user = options.user
    const limit = options.limit || 10
    const before = options.before || Math.floor(Date.now() / 1000)

    const urlToFetch = () => {
      const base = `${Messages.uri()}/chat/group/${channel}/messages?limit=${limit}&before=${before}`
      if (options.since) {
        return base + '&since=' + options.since
      } else {
        return base
      }
    }

    return new Request(urlToFetch())
      .auth({email: user.email, token: user.token})
      .get()
  }

  /**
   * Send a message to specified channel
   * @param {object} options
   * @param {string} options.channel - channel to send to
   * @param {User} options.user - User to send from
   * @param {string} options.message - message content to send
   * @param {number} [options.timestamp] - unix timestamp denoting time sent
   * @return {Promise<object>}
   * @example
   * Messages.send({
   *      channel: 'channel',
   *      user: aUser,
   *      message: 'Test Message'
   *    }).then((message) => {
   *      console.log(message)
   *    }).catch((e) => {
   *      console.error(e)
   *    })
   */
  static send (options) {
    if (!options || !options.channel || !options.user || !options.message) {
      return Promise.reject(new Error('No Channel, User, Options, or Message'))
    }

    const channel = options.channel
    const user = options.user
    const sender = options.user.id
    const content = options.message
    const timestamp = options.timestamp || Math.floor(Date.now() / 1000)

    return new Request(`${Messages.uri()}/chat/group/${channel}/messages`)
      .auth({ email: user.email, token: user.token })
      .post({
        body: JSON.stringify({
          group_name: channel,
          email: user.email,
          sender: sender,
          content: content,
          timestamp: timestamp
        })
      })
  }

  /**
   * Creates a channel
   * @param {object} options
   * @param {string} options.channel - channel name
   * @param {User} options.user - User to create from
   * @param {number[]} options.members - list of member ids to be in channel
   * @return {Promise<object>}
   * @example
   * Messages.createChannel({
   *      channel: 'channel',
   *      user: aUser,
   *      members: [1, 3, 5]
   *    }).then((message) => {
   *      console.log(message)
   *    }).catch((e) => {
   *      console.error(e)
   *    })
   */
  static createChannel (options) {
    if (!options || !options.channel || !options.user || !options.members) {
      return Promise.reject(new Error('No Channel, User, Options, or members'))
    }

    const channel = options.channel
    const user = options.user
    const members = options.members

    return new Request(`${Messages.uri()}/chat/group`)
      .auth({ email: user.email, token: user.token })
      .path({
        body: JSON.stringify({
          group_name: channel,
          email: user.email,
          members: members
        })
      })
  }

  /**
   * Updates a channels data
   * @param {object} options
   * @param {string} options.channel - channel name
   * @param {User} options.user
   * @param {number[]} [options.members] - list of member ids to be in channel
   * @param {object} [options.metadata] - object with metadata
   * @return {Promise<object>}
   * @example
   * Messages.updateChannel({
   *      channel: 'channel',
   *      user: aUser,
   *      members: [1, 3, 5]
   *    }).then((message) => {
   *      console.log(message)
   *    }).catch((e) => {
   *      console.error(e)
   *    })
   */
  static updateChannel (options) {
    if (!options || !options.channel || !options.user || !(options.members || options.metadata)) {
      return Promise.reject(new Error('No Channel, User, Options, or members / metadata'))
    }

    const channel = options.channel
    const user = options.user
    const members = options.members
    const metadata = options.metadata

    let body = {
      group_name: channel,
      email: user.email
    }

    if (members) {
      body.members = members
    }

    if (metadata) {
      body.metadata = metadata
    }

    return new Request(`${Messages.uri()}/chat/group`)
      .auth({ email: user.email, token: user.token })
      .post({ body: JSON.stringify(body) })
  }

  static uri () {
    return ''
  }
}

/**
 * @typedef {object} Message
 * @property {string} sender representing the id of the sender.
 * @property {string} content representing the message content
 * @property {string} group_name representing the channel
 * @property {number} timestamp representing the time sent
 */
