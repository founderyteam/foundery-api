import querystring from 'querystring'

class Exporter {
  constructor (mapping, obj) {
    this._mapping = mapping
    this._obj = obj
  }

  object () {
    this._output = Object.assign({}, this._withObj || {}, this._mapping(this._obj))
    return this
  }

  with (obj) {
    this._withObj = Object.assign({}, this._withObj || {}, obj)

    return this
  }

  json () {
    if (!this._output) {
      return this.object().json()
    }

    this._output = JSON.stringify(this._output)

    return this
  }

  querystring () {
    if (!this._output) {
      return this.object().querystring()
    }

    this._output = querystring.stringify(this._output)

    return this
  }

  ify () {
    if (!this._output) {
      return this.object().ify()
    }

    return this._output
  }
}

/**
 * Exportable facilitates stringification of User and Idea objects
 */
export default class Exportable {
  /**
   * Represents an exportable object
   * @param {function} mapping [a function that maps an object scope to a json object]
   */
  constructor (mapping) {
    this._mapping = mapping
  }

  exprt () {
    return this.export()
  }

  ex () {
    return this.export()
  }

  export () {
    return new Exporter(this._mapping, this)
  }
}
