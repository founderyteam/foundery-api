/* eslint no-undef: "off" */
import 'fetch-everywhere'
import FounderyInstance from './foundery-instance'

export default class Request {
  static fetch (uri, params) {
    let firstAction = Promise.resolve()
    console.log(params.headers)
    if (!params.headers || !params.headers['x-instance-token']) {
      firstAction = FounderyInstance.shared().getDefaultInstance().then(function () {
        params.headers = params.headers || {}
        params.headers['x-instance-token'] = FounderyInstance.shared().getInstanceToken()
      })
    }
    return firstAction
      .then(() => fetch(uri, params))
  }

  static verifyResponse (resp) {
    if (!(resp) || resp.success === false) {
      return Promise.reject(new Error(resp.message || 'Failure'))
    } else {
      return Promise.resolve(resp.payload)
    }
  }

  /**
   * [Represents a request object]
   * @constructor
   * @param url [the url of the request]
   * @param {string} [instanceToken] - the token holding instance information
  */
  constructor (url, instanceToken = FounderyInstance.shared().getInstanceToken()) {
    this._url = url
    this._noVerify = false
    this._instanceToken = instanceToken
    this._headers = {
      'x-instance-token': instanceToken,
      'x-web-token': 'none'
    }
  }

  /**
   * [Adds auth headers to request]
   * @return {Request} [returns the request]
  */
  auth (options = {}) {
    this._usesAuth = true
    this._headers = Object.assign({}, this._headers, {
      'x-auth-email': options.email,
      'x-web-token': options.token,
      'x-instance-token': this._instanceToken,
      'Content-Type': 'application/json'
    })
    return this
  }

  /**
   * [turns verification off for request response]
   * @return {Request} [returns the request]
   */
  noVerify () {
    this._noVerify = true
    return this
  }

  /**
   * [creates and sends a POST request]
   * @param {object} options [contains the request options (fetch), and token and email]
   * @return {Promise}         [the result promise]
   */
  post (options = {}) {
    if (!this._url) {
      return Promise.reject(new Error('No Url'))
    }

    const params = options || {}
    params.method = 'POST'

    params.headers = JSON.parse(JSON.stringify(Object.assign({}, this._headers || {}, options.headers)))

    let verifyPromise = Request.verifyResponse
    if (this._noVerify) {
      verifyPromise = resp => resp
    }
    return Request.fetch(this._url, params)
      .then(resp => resp.json())
      .then(verifyPromise)
  }

  /**
   * [creates and sends a DELETE request]
   * @param {object} options [contains the request options (fetch), and token and email]
   * @return {Promise}         [the result promise]
   */
  delete (options = {}) {
    if (!this._url) {
      return Promise.reject(new Error('No Url'))
    }

    const params = options || {}
    params.method = 'DELETE'

    params.headers = Object.assign({}, this._headers || {}, options.headers)

    let verifyPromise = Request.verifyResponse
    if (this._noVerify) {
      verifyPromise = resp => resp
    }

    return Request.fetch(this._url, params)
      .then(resp => resp.json())
      .then(verifyPromise)
  }

  /**
   * [creates and sends a GET request]
   * @param {object} options [contains the request options (fetch), and token and email]
   * @return {Promise}         [the result promise]
   */
  get (options = {}) {
    const params = options || {}
    params.method = 'GET'

    params.headers = Object.assign({}, this._headers || {}, options.headers)

    let verifyPromise = Request.verifyResponse
    if (this._noVerify) {
      verifyPromise = resp => resp
    }

    return Request.fetch(this._url, params)
      .then(resp => resp.json())
      .then(verifyPromise)
  }
}
