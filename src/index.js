import 'es6-promise/auto'
import FounderyInstance from './foundery-instance'

/**
 * Constructs the default foundery object
 * @param {string} uri - the uri of the api
 * @return {FounderyInstance}
 * @constructor
 * @example
 * import Foundery from 'foundery-js-api';
 * const foundery = Foundery(<uri>)
 * const {User, Idea} = foundery
 */
export default function Foundery (uri) {
  return new FounderyInstance(uri)
};
